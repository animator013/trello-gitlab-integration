# Trello & GitLab Integration

I am using for dev environment ts-node library, compiles typescript on the fly.

`npm run dev`

To build the project write this command:

`npm run build`

After successful build you need to copy `package.json` and `config.json` to the root of the project and then run `npm install`. This should be automated somehow.

You need to fill in the `apiKey` and `token` in the `config.json` file.

## Docker

You can run server with docker: `docker run --name trello-gitlab -d -e TRELLO_APIKEY=$TRELLO_APIKEY -e TRELLO_TOKEN=$TRELLO_TOKEN registry.gitlab.com/animator013/trello-gitlab-integration`

## Gitlab Setup

In your project navigate to `Settings > Integrations > Custom Issue Tracker`. This will transform issue ids to link to trello cards.

`HOST` = url where you host this middleware

`BOARDID` = your board id from trello.com

![Integration 1](https://i.imgur.com/Ii5DY5j.png)

Then navigate to `Settings > Integrations > Add web hook`. This is for gitlab push notifications.
![Integration 2](https://i.imgur.com/997wPn4.png)

## What needs to be done

- dev dependencies and dependencies
- gitlab token handling
- better build
- better everything
- refactor what is not needed for this tiny project (I recycled it from another project of mine)

