FROM node:8-alpine as builder

WORKDIR /trello

COPY . ./

RUN npm install

RUN npm run build

FROM node:8-alpine

WORKDIR /trello

COPY --from=builder /trello/*.json /trello/build/compiled/ ./

ENV NODE_ENV=production

RUN npm install

CMD npm run prod

EXPOSE 5000
