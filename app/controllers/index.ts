import {Router} from 'express';
import * as request from 'request';
import * as _ from 'lodash';
import {Holders} from "../core/holders";
let router : Router = Router();

const apiKey = process.env.TRELLO_APIKEY || Holders.config.trello.apiKey;
const token = process.env.TRELLO_TOKEN || Holders.config.trello.token;


router.get('/', function(req, res, next) {
    res.send({trelloIntegration: 1});
});

router.get('/favicon.ico', function(req, res, next) {
    res.status(200).send(null);
});

const refParser = (ref) => ref.replace(/^refs\/(?:tags|heads)\/(.+)$/, '$1');
router.get('/trello/:boardId/:id', function(req, res, next) {
    request.get({json: true, url: `https://api.trello.com/1/boards/${req.params.boardId}/cards/?fields=idShort&key=${apiKey}&token=${token}`}, function (e, r, response) {
        if (e) {
            res.send({error: true});
            return;
        }

        let foundCard: any = _.find(response, {idShort: parseInt(req.params.id)});
        if (foundCard) {
            res.redirect(`https://trello.com/c/${foundCard.id}`);
        } else {
            res.send({notFound: true});
        }
    });
});

router.post('/gitlab/push/hook/:boardId',function(req, res, next) {
    let str;
    if (req.body) {
        if (req.body.commits) {
            if (req.body.commits.length) {
                let cards = {};
                console.log(`[GITLAB HOOK] Searching for cards on board ${req.params.boardId}.`);
                request.get({json: true, url: `https://api.trello.com/1/boards/${req.params.boardId}/cards/?fields=idShort&key=${apiKey}&token=${token}`}, function (e, r, response) {
                    if (e) {
                        res.send({error: true});
                        return;
                    }
                    console.log("[GITLAB HOOK] Cards successfully downloaded. Getting through gitlab commits...");
                    _.each(req.body.commits, (commit) => {
                        let res = commit.message.match(/^#(\d+)(.*)/);
                        if (res) {
                            if (res.length == 3) {
                                console.log(`[GITLAB HOOK] Found commit with issueId ${parseInt(res[1])}. Looking for corresponding card.`);
                                let foundCard: any = _.find(response, {idShort: parseInt(res[1])});
                                if (foundCard) {
                                    console.log(`[GITLAB HOOK] The card has been found. Hash: ${foundCard.id}. Grouping cards by hash.`);
                                    if (!cards[foundCard.id]) cards[foundCard.id] = [];
                                    cards[foundCard.id].push({
                                        commitUrl: commit.url,
                                        commitMessage: commit.message,
                                        author: commit.author
                                    });
                                }
                            }
                        }
                    });
                    let cardsHashes = _.keys(cards);
                    _.each(cardsHashes, (card) => {
                        console.log(`[GITLAB HOOK] Pushing through API to trello.`);
                        str = "";
                        str += `**${req.body.user_name}** pushed ${cards[card].length} commits to branch [**${refParser(req.body.ref)}**](${(req.body.project.web_url || req.body.project.homepage)}/commits/${refParser(req.body.ref)}).`;
                        _.each(cards[card], (commit) => {
                            str += `\n>[${commit.commitMessage}](${commit.commitUrl})`;
                        });
                        request.post({json: true, url: `https://api.trello.com/1/cards/${card}/actions/comments?text=${encodeURI(str).replace(/#/g, "%23")}&key=${apiKey}&token=${token}`}, function(e, r, response) {
                            if (e) {
                                console.log(`[GITLAB HOOK] Push to trello was not successful. Error: ${e}`);
                            }
                            console.log(`[GITLAB HOOK] Pushed to trello. Response: ${response}`);
                        });
                    });
                });
            }
        }
    }
    res.send({success: true});
});

export = router;